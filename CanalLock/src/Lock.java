/**
 * The actual lock class that acts as a monitor. The lock can contain a vessel,
 * and raise and drain it's water level.
 * @author jordansteele
 *
 */

public class Lock extends VesselHolder {

	private WaterLevel waterLevel; // keeps track of the water level in the lock
	private int vesselsInSystem;
	
	public static enum WaterLevel {
		HIGH,
		LOW
	}
	
	/**
	 * Constructor.
	 */
	public Lock() {
		super();
		waterLevel = WaterLevel.LOW;
		vesselsInSystem = 0;
	}
	
	/**
	 * Receives a new vessel to the system that will thus be going up. Waits on
	 * the conditions that the lock is empty, the water level is low and there
	 * is no vessel currently on route to the lock (from the last section).
	 * @param vessel The new vessel to the system.
	 */
	public synchronized void receiveVesselToGoUp(Vessel vessel) {
		while (currVessel != null || waterLevel == WaterLevel.HIGH || 
				onRouteVessel != null || vesselsInSystem >= Param.SECTIONS) {
			waitAndHandleException();
		}
		System.out.printf(Strings.ENTER_LOCK_UP, vessel.getId());
		currVessel = vessel;
		waterLevel = WaterLevel.HIGH;
		vesselsInSystem++;
		notifyAll();
	}
	
	/**
	 * Receives the vessel that was on route to the lock from the last section. 
	 * Waits on the conditions that the lock is empty, the water level is high,
	 * and there is actually an on route vessel.
	 */
	public synchronized void receiveVesselToGoDown() {
		while (currVessel != null || waterLevel == WaterLevel.LOW 
				|| onRouteVessel == null) {
			waitAndHandleException();
		}
		System.out.printf(Strings.ENTER_LOCK_DOWN, onRouteVessel.getId());
		currVessel = onRouteVessel;
		onRouteVessel = null;
		waterLevel = WaterLevel.LOW;
		notifyAll();
	}
	
	/**
	 * Departs the current vessel in the lock from the system. Waits on the
	 * conditions that the lock has a vessel and the water level is low.
	 */
	public synchronized void departCurrVesselFromSystem() {
		while (currVessel == null || waterLevel == WaterLevel.HIGH) {
			waitAndHandleException();
		}
		System.out.printf(Strings.DEPART_LOCK, currVessel.getId());
		currVessel = null;
		vesselsInSystem--;
		notifyAll();
	}
	
	/**
	 * Sends the current vessel to the first section. Waits on the conditions
	 * that the lock has a vessel, the water level is high and whatever
	 * conditions need to be satisfied for the first section.
	 * @param toSec
	 */
	public synchronized void sendCurrVesselToFirstSection(Section toSec) {
		while (currVessel == null || waterLevel == WaterLevel.LOW || 
				!toSec.prepareToReceive(currVessel)) {
			waitAndHandleException();
		}
		System.out.printf(Strings.LEAVE_FROM_LOCK, currVessel.getId());
		currVessel = null;
		notifyAll();
	}
	
	/**
	 * Waits for the lock to be empty and then changes the water level.
	 */
	public synchronized void changeWaterLevel() {
		while (currVessel != null) {
			waitAndHandleException();
		}
		switch (waterLevel) {
			case HIGH:
				System.out.printf(Strings.DRAIN_LOCK);
				waterLevel = WaterLevel.LOW;
				break;
			case LOW:
				System.out.printf(Strings.FILL_LOCK);
				waterLevel = WaterLevel.HIGH;
				break;
			default:
				System.err.println("No such value for water level.");
				System.exit(-1);
				break;
		}
		notifyAll();
	}
}
