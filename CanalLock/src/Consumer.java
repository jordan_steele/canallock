/**
 * Removes vessels that are ready to depart, that is, the lock has an outbound 
 * vessel and the chamber's water level is low. The class thus simply waits
 * for a period of time and then tries to depart a vessel. Blocking on the above
 * conditions is handled by the monitor (the lock).
 * @author jordansteele
 *
 */

public class Consumer extends ActiveComponent {

	private Lock lock; // the lock to consume vessels from
	
	/**
	 * Constructor
	 * @param lock The lock to consume vessels from
	 */
	public Consumer(Lock lock) {
		this.lock = lock;
	}
	
	/**
	 * Continuously waits an amount of time and then tries to depart a vessel 
	 * from the lock
	 */
	@Override
	public void run() {
		while (true) {
			waitFor(Param.departureLapse());
			lock.departCurrVesselFromSystem();
		}
	}
	
}