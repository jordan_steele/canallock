/**
 * Picks up vessels from section i and delivers them to the successor section, 
 * subject to the constraints.
 * @author jordansteele
 *
 */

public class Tug extends ActiveComponent {

	private Section fromSec; // the section to take tugs from
	private Section toSec; // the section to take tugs to
	
	/**
	 * Constructor.
	 * @param fromSec The section to take tugs from
	 * @param toSec The section to take tugs to
	 */
	public Tug(Section fromSec, Section toSec) {
		this.fromSec = fromSec;
		this.toSec = toSec;
	}
	
	/**
	 * Tells the from section to send it's current vessel to the next section, 
	 * waits for the towing time, and then tells the to section to receive
	 * the vessel.
	 */
	@Override
	public void run() {
		while (true) {
			fromSec.sendCurrVesselToNextHolder(toSec);
			waitFor(Param.TOWING_TIME);
			toSec.receiveVessel();
		}
	}
}
