/**
 * Abstract class for active components to derive from. Active components being
 * threads that act on the monitors in the system.
 * @author jordansteele
 *
 */

public abstract class ActiveComponent extends Thread {

	/**
	 * Wrapper method for exception handling for Thread.sleep
	 * @param time Time in millis to sleep for
	 */
	protected void waitFor(int time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
}
