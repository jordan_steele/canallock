/**
 * The actual section class that acts as a monitor. A section can hold a vessel
 * and receive and send vessels on to the next section.
 * @author jordansteele
 *
 */

public class Section extends VesselHolder {

	private int sectionID; // the id of this section
	
	/**
	 * Constructor
	 * @param sectionID The id of this section
	 */
	public Section(int sectionID) {
		super();
		this.sectionID = sectionID;
	}
	
	/**
	 * Receives the vessel that was on route to this section. Waits on the 
	 * conditions that the section is empty, and there is actually an on route 
	 * vessel.
	 */
	public synchronized void receiveVessel() {
		while (currVessel != null || onRouteVessel == null) {
			waitAndHandleException();
		}
		System.out.printf(Strings.ENTER_SECTION, onRouteVessel.getId(), 
				sectionID);
		currVessel = onRouteVessel;
		onRouteVessel = null;
		notifyAll();
	}
	
	/**
	 * Sends the current vessel to the next holder. Waits on the conditions
	 * that the section has a vessel, and whatever conditions need to be 
	 * satisfied for the next holder.
	 * @param toSec
	 */
	public synchronized void sendCurrVesselToNextHolder(VesselHolder vesHolder){
		while (currVessel == null || !vesHolder.prepareToReceive(currVessel)){
			waitAndHandleException();
		}
		System.out.printf(Strings.LEAVE_FROM_SECTION, currVessel.getId(), 
				sectionID);
		currVessel = null;
		notifyAll();
	}
	
}