/**
 * Picks up vessels from the last section and tows them to the lock, subject to
 * the constraints that the last section is occupied, the lock is unoccupied, 
 * and the water level is high.
 * @author jordansteele
 *
 */

public class Return_tug extends ActiveComponent {

	private Section fromSec; // the section to tow vessels from (last section)
	private Lock lock; // the lock to tow vessels to
	
	/**
	 * Constructor.
	 * @param fromSec The section to tow vessels from (last section)
	 * @param lock The lock to tow vessels to
	 */
	public Return_tug(Section fromSec, Lock lock) {
		this.fromSec = fromSec;
		this.lock = lock;
	}
	
	/**
	 * Continuously attempts to send a vessel from the last section to the lock,
	 * then waits to simulate towing time, and then drops the vessel of at the 
	 * lock.
	 */
	@Override
	public void run() {
		while (true) {
			fromSec.sendCurrVesselToNextHolder(lock);
			waitFor(Param.TOWING_TIME);
			lock.receiveVesselToGoDown();
		}
	}
}
