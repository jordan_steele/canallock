/**
 * Generates new vessels that want to enter; hands them to the lock, subject to
 * the constraint that the lock is unoccupied and the chamber's water level is 
 * low. These constraints are handled by the monitor (the lock). 
 * @author jordansteele
 *
 */

public class Producer extends ActiveComponent {

	private Lock lock; // the lock to hand vessels to 
	
	/**
	 * Constructor.
	 * @param lock The lock to hand vessels to 
	 */
	public Producer(Lock lock) {
		this.lock = lock;
	}
	
	/**
	 * Waits for a period of time and then tells the lock the receive a new 
	 * vessel.
	 */
	@Override
	public void run() {
		while (true) {
			waitFor(Param.arrivalLapse());
			lock.receiveVesselToGoUp(Vessel.getNewVessel());
		}
	}
	
}
