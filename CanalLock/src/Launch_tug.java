/**
 * Picks up vessels from the lock and tows them to the first section, subject to 
 * the constraints that there is an inbound vessel in the lock and the first 
 * section is unoccupied. The constraints on leaving are handled by the 
 * monitors. This class thus simply takes a vessel from a lock when possible,
 * waits for the towing time, and then hands the vessel to the first section.
 * @author jordansteele
 *
 */

public class Launch_tug extends ActiveComponent {

	private Lock lock; // the lock to take vessels from 
	private Section toSec; // the section to drop vessels off at
	
	/**
	 * Constructor
	 * @param lock The lock to take vessels from
	 * @param toSec The section to drop vessels off at
	 */
	public Launch_tug(Lock lock, Section toSec) {
		this.lock = lock;
		this.toSec = toSec;
	}
	
	/**
	 * Continuously attempts to send a vessel from the lock to the first 
	 * section, then waits to simulate towing time, and then drops the vessel of 
	 * at the first section.
	 */
	@Override
	public void run() {
		while (true) {
			lock.sendCurrVesselToFirstSection(toSec);
			waitFor(Param.TOWING_TIME);
			toSec.receiveVessel();
		}
	}
	
}