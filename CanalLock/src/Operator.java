/**
 *  Inspects the lock at random intervals and, once the lock is unoccupied, 
 *  changes the chamber's water level, that is, fills or drains the chamber.
 * @author jordansteele
 *
 */

public class Operator extends ActiveComponent {

	private Lock lock; // the lock to drain / fill
	
	/**
	 * Constructor.
	 * @param lock The lock to drain / fill
	 */
	public Operator(Lock lock) {
		this.lock = lock;
	}
	
	/**
	 * Continuously waits a period of time and then attempts to change the 
	 * water level of the lock
	 */
	@Override
	public void run() {
		while (true) {
			waitFor(Param.operateLapse());
			lock.changeWaterLevel();
		}
	}
	
}