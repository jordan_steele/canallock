/**
 * Abstract class from which locks and sections extend.
 * @author jordansteele
 *
 */

public abstract class VesselHolder {

	protected Vessel currVessel; // current vessel in the holder, null if none
	protected Vessel onRouteVessel; // the vessel on route to this holder, null 
									// if none
	
	/**
	 * Constructor.
	 */
	public VesselHolder() {
		currVessel = null;
		onRouteVessel = null;
	}
	
	/**
	 * Simple wrapper method to allow uniform handling of exceptions on wait.
	 */
	protected void waitAndHandleException() {
		try {
			wait();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Called by other holders to check if this holder is prepared to receive
	 * a vessel and to then set this vessel to receive a vessel if it is.
	 * @param vessel The vessel this holder will be receiving
	 * @return Simply returns true once complete
	 */
	public synchronized boolean prepareToReceive(Vessel vessel) {
		while (currVessel != null || onRouteVessel != null) {
			waitAndHandleException();
		}
		onRouteVessel = vessel;
		notifyAll();
		return true;
	}
}
