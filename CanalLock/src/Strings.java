/**
 * A class to store all the user facing strings in the system.
 * @author jordansteele
 *
 */

public class Strings {
	
	// Lock.java
	public static final String ENTER_LOCK_UP = "[%d] enters lock to go up\n";
	public static final String ENTER_LOCK_DOWN = 
			"[%d] enters lock to go down\n";
	public static final String DEPART_LOCK = "[%d] departs\n";
	public static final String LEAVE_FROM_LOCK = "[%d] leaves the lock\n";
	public static final String DRAIN_LOCK = "Chamber drains\n";
	public static final String FILL_LOCK = "Chamber fills\n";
	
	// Section.java
	public static final String ENTER_SECTION = "[%d] enters section %d\n";
	public static final String LEAVE_FROM_SECTION = "[%d] leaves section %d\n";
}
